#[derive(Debug)]
pub struct EtymologicalRelationShip<'a> {
    pub from: Word<'a>,
    pub relationship: Relationship,
    pub to: Word<'a>,
}

impl<'a> EtymologicalRelationShip<'a> {
    pub fn new(s: &'a str) -> Option<Self> {
        let mut columns = s.split("\t");
        let from = Word::new(columns.next()?)?;
        let relationship = Relationship::new(columns.next()?);
        let to = Word::new(columns.next()?)?;
        Some(EtymologicalRelationShip {
            from,
            relationship,
            to,
        })
    }
}
#[derive(Debug)]
pub enum Relationship {
    EtymologicalOriginOf,
    HasDerivedForm,
    Etymology,
    Variant,
}
use Relationship::*;
impl Relationship {
    fn new(s: &str) -> Self {
        match s {
            "rel:etymological_origin_of" => EtymologicalOriginOf,
            "rel:has_derived_form" | "rel:is_derived_from" | "rel:derived" => HasDerivedForm,
            "rel:etymology" | "rel:etymologically_related" | "rel:etymologically" => Etymology,
            "rel:variant:orthography" => Variant,
            _ => panic!(),
        }
    }
}
#[derive(Debug)]
pub struct Language<'a>(pub &'a str);
#[derive(Debug)]
pub struct Word<'a> {
    pub language: Language<'a>,
    pub word: &'a str,
}

impl<'a> Word<'a> {
    fn new(s: &'a str) -> Option<Self> {
        let mut pair = s.split(":").map(|x| x.trim());
        let language = Language(pair.next()?.trim());
        let word = pair.next()?;
        Some(Self { language, word })
    }
}
